## An offline dictionary app based on wordnet DB ##

**Steps to use**
 - git clone https://gitlab.com/aaj013/dictionary.git
 - source dictionary/bin/activate
 - run **python download_wordnet.py** & check everything fine (TODO what if something went wrong)
 - use **python runthis.py <word_to_search>**
 - open **output.csv** using LibreOffice Calc or any equivalent spreadsheet tools
