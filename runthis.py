from nltk.corpus import wordnet as wn
import sys,csv,os,json

def main():
	#checking whether input exists
	if(len(sys.argv) != 2):
		print("Invalid Format! Try: " + "\033[1m" + "python wordnet.py <word_to_search>" + "\033[0m")
		return
	input_string = sys.argv[1]
	record = ['"' + input_string + '"']
	output_file_name = 'output.csv'
	wordset = wn.synsets(input_string) #get synonyms list
	for word in wordset:
		word_meaning = '"' + str(word.definition()) + '"'
		record.append(word_meaning)
	if os.path.exists(output_file_name) != 1: #if no file create one with headings
		with open(output_file_name,'w') as csv_file:
			writer = csv.writer(csv_file)
			writer.writerow(['word','meanings'])
	with open(output_file_name,'a') as csv_file: #appending word and meanings to output
		out_str = ",".join(record)
		csv_file.write(out_str+"\n")
	print("\n" + out_str + "\n")

if __name__ == '__main__':
    main()